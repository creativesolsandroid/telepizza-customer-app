package com.cs.telepizza;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class InsideMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inside_menu);
    }
}
