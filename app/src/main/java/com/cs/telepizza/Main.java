package com.cs.telepizza;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class Main extends AppCompatActivity {

    ImageView morder_now;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        morder_now= (ImageView) findViewById(R.id.order_now);
        morder_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a=new Intent(Main.this,Menu.class);
                startActivity(a);
            }
        });
    }
}
